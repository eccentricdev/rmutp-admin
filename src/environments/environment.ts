// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  rsuAppApi:'https://rsu-app-api.71dev.com/uat/',
  apiRegister:'https://rsu-reg-api.71dev.com/v2',
  apiversion:'v1',
  apiCommonApi: 'https://rsu-common-api.71dev.com/api',
  openIdApi: 'https://iam71-api.71dev.com/openid',
  eduApi:'https://education-api.71dev.com/dev/api/',
  clientSettings: {
    authority: 'https://rmutp-iam.71dev.com/',
    client_id: 'rmutp-admin-web-client-dev',
    redirect_uri: 'http://localhost:4200/auth-callback',
    post_logout_redirect_uri: 'http://localhost:4200',
    response_type:"id_token token",
    scope:"rmutpadmin openid profile",
    filterProtocolClaims: true,
    loadUserInfo: true
  },
  };
  
  /*
   * For easier debugging in development mode, you can import the following file
   * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
   *
   * This import should be commented out in production mode because it will have a negative impact
   * on performance if an error is thrown.
   */
  // import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
  