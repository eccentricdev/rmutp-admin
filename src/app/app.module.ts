import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ExtraOptions, PreloadAllModules, RouterModule } from '@angular/router';
import { MarkdownModule } from 'ngx-markdown';
import { FuseModule } from '@fuse';
import { FuseConfigModule } from '@fuse/services/config';
import { FuseMockApiModule } from '@fuse/lib/mock-api';
import { CoreModule } from 'app/core/core.module';
import { appConfig } from 'app/core/config/app.config';
import { mockApiServices } from 'app/mock-api';
import { LayoutModule } from 'app/layout/layout.module';
import { AppComponent } from 'app/app.component';
import { appRoutes } from 'app/app.routing';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { localStorageSync } from 'ngrx-store-localstorage';

const routerConfig: ExtraOptions = {
    scrollPositionRestoration: 'enabled',
    // preloadingStrategy       : PreloadAllModules
};

export function debug(reducer: ActionReducer<any>): ActionReducer<any> {
    return function(state, action) {
      // !environment.production ? console.log('state', state) : {} ; 
      return reducer(state, action);
    };
  }
  
  export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
    return localStorageSync({
      keys: ['rsu','user','stepper','programe'],
      rehydrate: true
    })(reducer);
  }


  export const metaReducers: MetaReducer<any>[] = [
    debug,
    // localStorageSyncReducer
  ];

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports     : [
        BrowserModule,
        BrowserAnimationsModule,
        RouterModule.forRoot(appRoutes, routerConfig),

        // Fuse, FuseConfig & FuseMockAPI
        FuseModule,
        FuseConfigModule.forRoot(appConfig),
        FuseMockApiModule.forRoot(mockApiServices),

        // Core module of your application
        CoreModule,

        // Layout module of your application
        LayoutModule,

        // 3rd party modules that require global configuration via forRoot
        MarkdownModule.forRoot({}),

        StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),

        EffectsModule.forRoot([]),

        StoreRouterConnectingModule.forRoot(),

        StoreModule.forRoot({}, {})
    ],
    bootstrap   : [
        AppComponent
    ]
})
export class AppModule
{
}


