import { LanguageService } from './../../../core/service/subject/language.service';
import { Component, OnInit } from '@angular/core';
import { createQueryStringFromObject } from 'app/shared/util/func';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit {

  items$ = new Observable()
  constructor(
    private languageSV:LanguageService
  ) { }

  ngOnInit(): void {
    this.items$ = this.languageSV.getAll()
  }

  search(query: string){
    let queryStr = createQueryStringFromObject(query)
    if (queryStr) {
      this.items$ = this.languageSV.query(`?${queryStr}`)
    } else {
      this.items$ = this.languageSV.getAll()
    }
  }

}
