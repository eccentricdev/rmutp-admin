import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { BaseList } from 'app/core/base/base-list';
import { LanguageService } from 'app/core/service/subject/language.service';
import { SkillService } from 'app/core/service/subject/skill.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseList implements OnInit,OnChanges {
  @Input() items: any
  displayedColumns = ['1','2','3','4','5','6']
  constructor(
    private router:Router,
    private cdRef: ChangeDetectorRef,
    private skillSV:SkillService,
    private languageSV:LanguageService
  ) { 
    super()
  }

  ngOnInit(): void {
    
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes?.items?.currentValue) {
      this.items = this.updateMatTable(changes.items.currentValue)
    } 
  }

  edit(id){
    // this.router.navigate(['app/fee-group/edit',id])
    // this.skillSV.subuid = id
    this.router.navigate(['/app/language/edit',id])
  }

  delete(id){
    Swal.fire({
      icon: 'warning',
      title: 'คุณต้องการลบข้อมูลนี้หรือไม่?',
      showCancelButton: true,
      confirmButtonText: `ตกลง`,
      cancelButtonText: `ยกเลิก`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.languageSV.deleteDate(id).subscribe(() => {
          Swal.fire('ลบข้อมูลเรียบร้อยแล้ว', '', 'success');
          this.items = this.languageSV.getAll();
          this.cdRef.detectChanges()
        });
      }
    });
  }
}
