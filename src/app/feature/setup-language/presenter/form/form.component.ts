import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseForm } from 'app/core/base/base-form';
import { LanguageService } from 'app/core/service/subject/language.service';
import { SweetalertService } from 'app/core/service/sweetalert.service';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent extends BaseForm implements OnInit {
  datainternal={
    language_code:null,
    language_name_th:null,
    language_name_en:null,
    status_id:null
  }
  constructor(
    public activeRouter: ActivatedRoute,
    public fb: FormBuilder,
    private router:Router,
    private swSV: SweetalertService,
    private cdRef: ChangeDetectorRef,
    private languageSV:LanguageService
  ) {
    super(fb,activeRouter);
  }

  ngOnInit(): void {
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.languageSV.get(this.id).subscribe((res: any) => {
          console.log(res);
          this.datainternal = res;
          this.cdRef.detectChanges();
        })
        break;

      case 'add':
        
      break
    }
  }

  save(){
    switch (this.state) {
      case 'edit':
        this.languageSV.update2(this.datainternal).pipe(
          tap(res => this.swSV.saveSuccess()),
          tap(() => this.clear()),
          tap(() => this.cdRef.detectChanges()),
          catchError(err => {
            this.swSV.error()
            return throwError(err)
          })
        ).subscribe()
        break;

      case 'add':
        console.log(this.datainternal)
        this.languageSV.add(this.datainternal).pipe(
          tap(res => this.swSV.saveSuccess()),
          tap(() => this.clear()),
          tap(() => this.cdRef.detectChanges()),
          catchError(err => {
            this.swSV.error()
            return throwError(err)
          })
        ).subscribe()
        break
    }
  }

  clear(){
    this.router.navigate(['/app/language'])
  }
}
