import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { LanguageService } from 'app/core/service/subject/language.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  @Output() onSearch = new EventEmitter<any>()
  language
  search={
    language_uid:null
  }
  constructor(
    private router:Router,
    private languageSV:LanguageService
  ) { }

  ngOnInit(): void {
    this.languageSV.getAll().subscribe((x:any)=>{
      this.language = x
    })
  }

  Search(){
    console.log(this.search)
    this.onSearch.emit(this.search)
  }

  add(){
    this.router.navigate(['/app/language/add'])
  }

  displaylanguage(id) {
    console.log(id)
    if(id) return this.language.find(language => language.language_uid === id)?.language_name_th
  }

}
