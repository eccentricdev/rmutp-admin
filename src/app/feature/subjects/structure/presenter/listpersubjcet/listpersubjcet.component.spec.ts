import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListpersubjcetComponent } from './listpersubjcet.component';

describe('ListpersubjcetComponent', () => {
  let component: ListpersubjcetComponent;
  let fixture: ComponentFixture<ListpersubjcetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListpersubjcetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListpersubjcetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
