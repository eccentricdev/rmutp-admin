import { filter } from 'rxjs/operators';
import { courses } from './../../../../../mock-api/apps/academy/data';
import { BaseForm, sortByProperty } from './../../../../../core/base/base-form';
import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { BaseList } from 'app/core/base/base-list';
import { SubjectService } from 'app/core/service/subject/subject.service';
import { CourseService } from 'app/core/service/subject/course.service';
import { createQueryStringFromObject } from 'app/shared/util/func';
import { CourseActivityService } from 'app/core/service/subject/course-activity.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-listpersubjcet',
  templateUrl: './listpersubjcet.component.html',
  styleUrls: ['./listpersubjcet.component.scss']
})
export class ListpersubjcetComponent extends BaseList implements OnInit,OnChanges {

  @Input() items: any
  itemDatas: any
  length
  // itemDatas:any = [{1:1}]
  displayedColumns = ['1','2','4']
  sub = {
    subject_uid:null
  }
  constructor(
    private router:Router,
    private cdRef: ChangeDetectorRef,
    private subjectSV:SubjectService,
    private coruseSV:CourseService,
    private courseAcAV:CourseActivityService
  ) {
    super();
  }

  ngOnInit(): void {
    console.log(this.subjectSV.Subjectuid)
    this.sub.subject_uid = this.subjectSV.Subjectuid
    let queryStr = createQueryStringFromObject(this.sub)
    console.log(queryStr)
    this.coruseSV.queryString(`${queryStr}`).subscribe((x:any)=>{
      console.log(x)
      this.itemDatas = x.sort((a,b) => a.row_no - b.row_no)
      this.itemDatas = this.itemDatas.filter(x => x.subject_uid != null)
    
      
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes?.items?.currentValue) {
      this.items = this.updateMatTable(changes.items.currentValue)
    } 
  }

  Search(){
    this.subjectSV.Subjectuid = this.subjectSV.Subjectuid
    this.router.navigate(['/app/structure-subject/add-structure'])
  }

  delete(id){
    Swal.fire({
      icon: 'warning',
      title: 'คุณต้องการลบข้อมูลนี้หรือไม่?',
      showCancelButton: true,
      confirmButtonText: `ตกลง`,
      cancelButtonText: `ยกเลิก`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.coruseSV.deleteDate(id).subscribe(() => {
          Swal.fire('ลบข้อมูลเรียบร้อยแล้ว', '', 'success');
        //  console.log(this.subjectSV.Subjectuid)
         let queryStr = createQueryStringFromObject(this.sub)
          this.coruseSV.queryString(`${queryStr}`).subscribe((x:any)=>{
            console.log(x)
            this.itemDatas = x.sort((a,b) => a.row_no - b.row_no)
            this.itemDatas = this.itemDatas.filter(x => x.subject_uid != null)
          })
          this.cdRef.detectChanges()
        });
      }
    });
  }
}
