import { QuestionsService } from './../../../../../core/service/subject/questions.service';
import { courses } from './../../../../../mock-api/apps/academy/data';
import { activities } from './../../../../../mock-api/pages/activities/data';

import { BaseForm } from './../../../../../core/base/base-form';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ChangeDetectorRef, OnChanges, SimpleChanges } from '@angular/core';
import { CourseService } from 'app/core/service/subject/course.service';
import { SubjectService } from 'app/core/service/subject/subject.service';
import { ActivityService } from 'app/core/service/subject/activity.service';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { catchError, concatMap, map, tap } from 'rxjs/operators';
import { Location } from '@angular/common';
import { throwError } from 'rxjs';
import { SweetalertService } from 'app/core/service/sweetalert.service';
import { CourseActivityService } from 'app/core/service/subject/course-activity.service';
import Swal from 'sweetalert2';
import { createQueryStringFromObject } from 'app/shared/util/func';

@Component({
  selector: 'app-create-structure',
  templateUrl: './create-structure.component.html',
  styleUrls: ['./create-structure.component.scss']
})
export class CreateStructureComponent extends BaseForm implements OnInit,OnChanges {

  // checksave = false
  datainternal = {
    course_activity_uid: null
  }
  questions = {
    status_id: null,
        created_by: null,
        created_datetime: null,
        updated_by: null,
        updated_datetime: null,
        search: null,
        owner_agency_uid: null,
        question_uid: null,
        question_name: null,
        correct_answer: null,
        correct_choice_uid: null,
        course_activity_uid: null,
        choices: null
  }
  activity
  form: FormGroup;
  form1: FormGroup;
  constructor(
    private swSV: SweetalertService,
    public location: Location,
    public activeRouter: ActivatedRoute,
    public fb: FormBuilder,
    private router: Router,
    private subjectSV: SubjectService,
    private coruseSV: CourseService,
    private activitySV: ActivityService,
    private coursesAcSV:CourseActivityService,
    private cdRef: ChangeDetectorRef,
    private quSV: QuestionsService
  ) {
    super(fb, activeRouter);
    this.form = this.fb.group({
      status_id: null,
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      search: null,
      owner_agency_uid: null,
      course_uid: null,
      subject_uid: null,
      course_code: null,
      course_short_name_en: null,
      course_short_name_th: null,
      course_name_th: null,
      course_name_en: null,
      course_image: null,
      row_no:null,
      course_activitys: this.fb.array([

      ]),
    })
  }
  ngOnChanges(changes: SimpleChanges): void {
   
  }

  createSubForm(value?) {
    if (value) {
      return this.fb.group({
        status_id: value.status_id,
        created_by: value.created_by,
        created_datetime: value.created_datetime,
        updated_by: value.updated_by,
        updated_datetime: value.updated_datetime,
        search: value.search,
        owner_agency_uid: value.owner_agency_uid,
        course_activity_uid: value.course_activity_uid,
        course_activity_code: value.course_activity_code,
        course_activity_short_name_en: value.course_activity_short_name_en,
        course_activity_short_name_th: value.course_activity_short_name_th,
        course_activity_name_th: value.course_activity_name_th,
        course_activity_name_en: value.course_activity_name_en,
        course_activity_image: value.course_activity_image,
        source_learning_video: value.source_learning_video,
        activity_uid: value.activity_uid,
        course_uid: value.course_uid,
        test_question_number: value.test_question_number,
        test_time:value.test_time,
        minimum_score_percent: value.minimum_score_percent,
        row_no: value.row_no,
        questions: value?.questions
  
      })
    }
    return this.fb.group({
      status_id: null,
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      search: null,
      owner_agency_uid: null,
      course_activity_uid: null,
      course_activity_code: null,
      course_activity_short_name_en: null,
      course_activity_short_name_th: null,
      course_activity_name_th: null,
      course_activity_name_en: null,
      course_activity_image: null,
      source_learning_video: null,
      activity_uid: null,
      course_uid: this.form.value.course_uid,
      test_question_number:0,
      test_time:null,
      minimum_score_percent: null,
      row_no: null
    })
  }

  _createSubForm(value?) {
    if (value) {
      return this.fb.group({
        status_id: null,
        created_by: null,
        created_datetime: null,
        updated_by: null,
        updated_datetime: null,
        search: null,
        owner_agency_uid: null,
        question_uid: null,
        question_name: null,
        correct_answer: null,
        correct_choice_uid: null,
        course_activity_uid: null,
        choices: null
      })
    }
    return this.fb.group({
      status_id: null,
        created_by: null,
        created_datetime: null,
        updated_by: null,
        updated_datetime: null,
        search: null,
        owner_agency_uid: null,
        question_uid: null,
        question_name: null,
        correct_answer: null,
        correct_choice_uid: null,
        course_activity_uid: null,
        choices: null
    })
  }
  addForm() {
    console.log(this.form.value)
    if (this.form.value.course_activitys == null) {
      // console.log("course_activitys == null")
      
    } else {
      // console.log("course_activitys == null")
      console.log(this.state)
      switch (this.state) {
        case 'edit':

          this.coruseSV.put(this.form.getRawValue()).pipe(
            tap(res => console.log(res)),
            // tap(res => this.swSV.saveSuccess()),
            // tap(() => this.location.back()),
            catchError(err => {
              this.swSV.error()
              return throwError(err)
            })
          ).subscribe()
          break;

        case 'add':
          console.log(this.form.value)
          if (this.form.value.course_activitys.length == 0) {
            console.log("bbdkhfb")
            this.form.get('subject_uid').setValue(this.subjectSV.Subjectuid)
            this.coruseSV.add(this.form.getRawValue()).pipe(
              tap(res => console.log(res)),
              // tap(res => this.swSV.saveSuccess()),
              
              tap((res:any) => this.router.navigate(['/app/structure-subject/structure/edit',res.course_uid])),
              tap(() => this.cdRef.detectChanges()),
              catchError(err => {
                this.swSV.error()
                return throwError(err)
              })
            ).subscribe()
            this.cdRef.detectChanges()
          }

          break
      }
    }
    let form = this.form.get('course_activitys') as FormArray
    form.push(this.createSubForm())

  }

  removeForm(index: number) {
    let form = this.form.get('course_activitys') as FormArray
    console.log(form.value)
    console.log(this.form.value.course_activitys[index].course_activity_uid)
    let id = this.form.value.course_activitys[index].course_activity_uid
    if(id){
      this.coursesAcSV.deleteDate(id).subscribe(() => {
        // Swal.fire('ลบข้อมูลเรียบร้อยแล้ว', '', 'success');
        // this.items = this.subjectSV.getAll();
        form.removeAt(index)
        // this.cdRef.detectChanges()
      });
    }else{
      form.removeAt(index)
    }
  
  }

  ngOnInit(): void {
    this.cdRef.detectChanges()
    console.log(this.subjectSV.Subjectuid)
    console.log(this.form.value)
    this.activitySV.getAll().subscribe((x:any) => {
      this.activity = x.filter(status => status.status_id == 1)
    })
    switch (this.state) {
      case 'edit':
        console.log(this.id)
        this.callType().pipe(
          concatMap(() => {
            return this.coruseSV.get(this.id).pipe(
              tap(res => console.log(res)),
              tap((res: any) => {
                this.form.patchValue(res)
                // this.form.setControl('course_activitys',this.fb.array([this.createSubForm(res)]))
                let form = this.form.get('course_activitys') as FormArray
                res.course_activitys.forEach(element => {
                  form.push(this.createSubForm(element))
                });
              })
            )
          })
        ).subscribe()
        break;

      case 'add':
        this.addForm()
        break
    }
  }



  add(item) {
    switch (this.state) {
      case 'edit':

        this.coruseSV.put(this.form.getRawValue()).pipe(
          tap(res => console.log(res)),
          // tap(res => this.swSV.saveSuccess()),
          // tap(() => this.location.back()),
          catchError(err => {
            this.swSV.error()
            return throwError(err)
          })
        ).subscribe()
        break;

      case 'add':
        console.log(this.form.value)
        if (this.form.value.course_activitys != null) {
          this.coruseSV.add(this.form.getRawValue()).pipe(
            tap(res => console.log(res)),
            // tap(res => this.swSV.saveSuccess()),
            // tap(() => this.location.back()),
            catchError(err => {
              this.swSV.error()
              return throwError(err)
            })
          ).subscribe()
        }

        break;
      }
    console.log(item.value)
    if (item.value.activity_uid == "f3d247be-e4f2-47ce-b94e-48e2de9f526f") {
      //lesson
      this.coruseSV.lesson = item.value
      // console.log(this.coruseSV.lesson)
      // console.log(this.coruseSV.lesson.course_activity_uid)
      this.datainternal.course_activity_uid = this.coruseSV.lesson.course_activity_uid
      console.log(this.datainternal.course_activity_uid)
      let queryStr = createQueryStringFromObject(this.datainternal)
      this.quSV.queryString(queryStr).subscribe((x:any)=>{
        this.quSV.qu = x
        console.log(this.quSV.qu)
      })
      this.router.navigate(['/app/structure-subject/lesson'])
    }
    else if (item.value.activity_uid == "3e47355d-c02a-4e30-8a9d-ead359a8cb96") {
      //Quiz
      this.coruseSV.lesson = item.value
      this.router.navigate(['/app/structure-subject/inform-quiz'])
    } else if (item.value.activity_uid == "7c93c33f-4361-4bf5-82fb-9a3042dd20ac") {
      //pre-post
      this.coruseSV.lesson = item.value
      this.router.navigate(['/app/structure-subject/inform'])
    }else if (item.value.activity_uid == "b26e52e6-d453-4db1-9df8-fc0fc759cd12") {
      //post
      this.coruseSV.lesson = item.value
      this.router.navigate(['/app/structure-subject/inform'])
    }

  }

  search(item) {
    console.log(item.value)
    switch (this.state) {
      case 'edit':

        this.coruseSV.put(this.form.getRawValue()).pipe(
          tap(res => console.log(res)),
          // tap(res => this.swSV.saveSuccess()),
          // tap(() => this.location.back()),
          catchError(err => {
            this.swSV.error()
            return throwError(err)
          })
        ).subscribe()
        break;

      case 'add':
        console.log(this.form.value)
        if (this.form.value.course_activitys != null) {
          this.coruseSV.add(this.form.getRawValue()).pipe(
            tap(res => console.log(res)),
            // tap(res => this.swSV.saveSuccess()),
            // tap(() => this.location.back()),
            catchError(err => {
              this.swSV.error()
              return throwError(err)
            })
          ).subscribe()
        }

        break;
      }
    if (item.value.activity_uid == "f3d247be-e4f2-47ce-b94e-48e2de9f526f") {
      //lesson
      this.coruseSV.lesson = item.value
      // console.log(this.coruseSV.lesson)
      // console.log(this.coruseSV.lesson.course_activity_uid)
      this.datainternal.course_activity_uid = this.coruseSV.lesson.course_activity_uid
      console.log(this.datainternal.course_activity_uid)
      let queryStr = createQueryStringFromObject(this.datainternal)
      this.quSV.queryString(queryStr).subscribe((x:any)=>{
        this.quSV.qu = x
        console.log(this.quSV.qu)
      })
      this.router.navigate(['/app/structure-subject/lesson'])
    }
    else if (item.value.activity_uid == "3e47355d-c02a-4e30-8a9d-ead359a8cb96") {
      //Quiz
      this.coruseSV.lesson = item.value
      this.router.navigate(['/app/structure-subject/inform-quiz'])
    } else if (item.value.activity_uid == "7c93c33f-4361-4bf5-82fb-9a3042dd20ac") {
      //pre-post
      this.coruseSV.lesson = item.value
      this.router.navigate(['/app/structure-subject/inform'])
    }else if (item.value.activity_uid == "b26e52e6-d453-4db1-9df8-fc0fc759cd12") {
      //post
      this.coruseSV.lesson = item.value
      this.router.navigate(['/app/structure-subject/inform'])
    }
  }

  callType() {
    return this.activitySV.getAll().pipe(
      map((res: any) => res.filter(r => r.status_id == 1)),
      tap((res: any) => this.activity = [...res])
    )
  }

  back(){
    this.router.navigate(['/app/structure-subject/list-structure'])
  }

  map(item,index){
    // console.log(item)
    console.log(item.value)
    let form = this.form.get('course_activitys') as FormArray
    console.log(this.form.value.course_uid)
    Swal.fire({
      icon: 'warning',
      title: 'คุณต้องการบันทึกข้อมูลนี้หรือไม่?',
      showCancelButton: true,
      confirmButtonText: `ตกลง`,
      cancelButtonText: `ยกเลิก`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.coruseSV.put(this.form.getRawValue()).pipe(
          tap(res => console.log(res)),
          tap(() =>  form.clear()),
          tap((res:any) =>  this.ngOnInit()),
          tap(() => this.cdRef.detectChanges()),
          catchError(err => {
            this.swSV.error()
            return throwError(err)
          })
          
        ).subscribe()
        this.cdRef.detectChanges()     }
    });


    
    
    // console.log(this.form1)
    // console.log(this.form.value)
    // this.cdRef.detectChanges()
    // this.callType().pipe(
    //   concatMap(() => {
    //     return this.coruseSV.get(this.id).pipe(
    //       tap(res => console.log(res)),
    //       tap((res: any) => {
    //         this.form.patchValue(res)
    //         // this.form.setControl('course_activitys',this.fb.array([this.createSubForm(res)]))
    //         let form = this.form.get('course_activitys') as FormArray
    //         console.log(form)
    //         res.course_activitys.forEach(element => {
    //           form.push(this.createSubForm(element))
    //         });
    //       })
    //     )
    //   })
    // ).subscribe()
  }
}
