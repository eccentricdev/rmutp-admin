import { BaseForm } from './../../../../../../core/base/base-form';
import { courses } from './../../../../../../mock-api/apps/academy/data';
import { BaseTiny } from './../../../../../../core/base/tinymce';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { UploadImgService } from 'app/core/service/upload/upload-img.service';
import { CourseService } from 'app/core/service/subject/course.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { CourseActivityService } from 'app/core/service/subject/course-activity.service';
import { QuestionsService } from 'app/core/service/subject/questions.service';
import { createQueryStringFromObject } from 'app/shared/util/func';

@Component({
  selector: 'app-lesson',
  templateUrl: './lesson.component.html',
  styleUrls: ['./lesson.component.scss']
})
export class LessonComponent extends BaseTiny implements OnInit {

  form
  datainternal = {
    course_activity_uid: null
  }
  data = []
  questions = {
    status_id: 1,
    created_by: null,
    created_datetime: null,
    updated_by: null,
    updated_datetime: null,
    search: null,
    owner_agency_uid: null,
    question_uid: null,
    question_name: null,
    correct_answer: null,
    correct_choice_uid: null,
    course_activity_uid: null,
    choices: []
  }
  CA = {
    status_id: null,
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      search: null,
      owner_agency_uid: null,
      course_activity_uid: null,
      course_activity_code: null,
      course_activity_short_name_en: null,
      course_activity_short_name_th: null,
      course_activity_name_th: null,
      course_activity_name_en: null,
      course_activity_image: null,
      source_learning_video: null,
      activity_uid: null,
      course_uid: null,
      test_question_number:0,
      test_time:null,
      minimum_score_percent: null,
      row_no: null
  }
  qu = {
    question_name: null,
  }
  flag = false
  constructor(
    public uploadSV: UploadImgService,
    private coruseSV: CourseService,
    private router: Router,
    private cdRef: ChangeDetectorRef,
    private courseACSV: CourseActivityService,
    private quSV: QuestionsService
  ) {
    super(uploadSV);
  }

  ngOnInit(): void {
    console.log(this.coruseSV.lesson)
    this.form = this.coruseSV.lesson
    // console.log(this.form)
    // this.form.questions = this.questions
    // console.log(this.form)
    // this.questions.course_activity_uid = this.form.course_activity_uid
    // this.datainternal.course_activity_uid = this.form.course_activity_uid
   
    // let queryStr = createQueryStringFromObject(this.datainternal)
    // this.quSV.queryString(queryStr).subscribe((x: any) => {
    //   console.log(x)
    //   this.data = x
    //   if(x.length != 0){
    //     this.questions = x
    //     console.log(this.questions)
    //     this.flag = true
    //   }else{
    //     this.questions.course_activity_uid = this.form.course_activity_uid
    //     console.log(this.questions)
    //   }
    //   this.cdRef.detectChanges()
    // })
    // this.cdRef.detectChanges()
    // console.log(this.questions)
  }

  save() {
    // this.form.course_activity_name_en = this.datainternal.course_activity_name_en
    // console.log(this.form)
    // this.courseACSV.put(this.form).subscribe((x)=>{
    //   console.log(x)
    //   Swal.fire({
    //     icon: 'success',
    //     text: 'บันทึกข้อมูลเรียบร้อย',
    //     confirmButtonText: `ตกลง`,
    //   }).then((result) => {
    //     if (result.isConfirmed) {
    //       this.router.navigate(['/app/subject'])
    //     }
    //   })
    // })
    // this.questions.question_name = this.qu.question_name
    // console.log(this.questions)
    // if(this.flag == true){
    //   console.log(this.flag)
    //   this.quSV.update2(this.questions[0]).subscribe((x) => {
    //     console.log(x)
    //     Swal.fire({
    //       icon: 'success',
    //       text: 'บันทึกข้อมูลเรียบร้อย',
    //       confirmButtonText: `ตกลง`,
    //     }).then((result) => {
    //       if (result.isConfirmed) {
    //         this.router.navigate(['/app/subject'])
    //       }
    //     })
    //   })
    // }else{
    //   console.log(this.flag)
    //   console.log(this.questions)
    //   this.quSV.add(this.questions).subscribe((x) => {
    //     console.log(x)
    //     Swal.fire({
    //       icon: 'success',
    //       text: 'บันทึกข้อมูลเรียบร้อย',
    //       confirmButtonText: `ตกลง`,
    //     }).then((result) => {
    //       if (result.isConfirmed) {
    //         this.router.navigate(['/app/subject'])
    //       }
    //     })
    //   })
    // }
    console.log(this.form)
    this.courseACSV.update2(this.form).subscribe((x:any)=>{
        console.log(x)
        Swal.fire({
          icon: 'success',
          text: 'บันทึกข้อมูลเรียบร้อย',
          confirmButtonText: `ตกลง`,
        }).then((result) => {
          if (result.isConfirmed) {
            this.router.navigate(['/app/structure-subject/structure/edit',this.form.course_uid])
          }
        })
    })
  }

  clear() {
    this.router.navigate(['/app/structure-subject/structure/edit',this.form.course_uid])
  }
}
