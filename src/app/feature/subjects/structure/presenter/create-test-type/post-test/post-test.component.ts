import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { BaseTiny } from 'app/core/base/tinymce';
import { ChoicesService } from 'app/core/service/subject/choices.service';
import { CourseActivityService } from 'app/core/service/subject/course-activity.service';
import { CourseService } from 'app/core/service/subject/course.service';
import { QuestionsService } from 'app/core/service/subject/questions.service';
import { UploadImgService } from 'app/core/service/upload/upload-img.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-post-test',
  templateUrl: './post-test.component.html',
  styleUrls: ['./post-test.component.scss']
})
export class PostTestComponent extends BaseTiny implements OnInit {
  choices:any={
    status_id: null,
    created_by: null,
    created_datetime: null,
    updated_by: null,
    updated_datetime: null,
    search: null,
    owner_agency_uid: null,
    choice_uid: null,
    choice_name: null,
    question_uid: null,
    is_checked:null

  }
  questions = {
    status_id: null,
    created_by: null,
    created_datetime: null,
    updated_by: null,
    updated_datetime: null,
    search: null,
    owner_agency_uid: null,
    question_uid: null,
    question_name: null,
    correct_answer: null,
    correct_choice_uid: null,
    course_activity_uid: null,
    choices:[this.choices]
  }
  test
  form
  course_activity_uid
  items: any 
  displayedColumns = ['1', '2', '3', '5']
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  rows: any = new MatTableDataSource<any>([]);
  constructor(
    public uploadSV: UploadImgService,
    private coruseSV: CourseService,
    private router: Router,
    private cdRef: ChangeDetectorRef,
    private courseACSV: CourseActivityService,
    private quSV: QuestionsService,
    private choiceSV:ChoicesService
  ) {
    super(uploadSV);
  }

  ngOnInit(): void {
    this.test = this.coruseSV.test
    if (this.quSV.qu_uid != null){
      console.log("x")
      this.quSV.get(this.quSV.qu_uid).subscribe((x: any) => {
        console.log(x)
        this.questions = x
        this.items = x.choices
        this.updateMatTable(this.items)
        console.log(this.questions)
      })
    }else{
      console.log(this.coruseSV.quiz)
      this.form = this.coruseSV.quiz
      // this.form.questions = this.questions
      this.coruseSV.lesson.course_activity_uid = this.course_activity_uid
      console.log(this.course_activity_uid)
      // this.quSV.queryString(this.course_activity_uid).subscribe((x: any) => {
      //   console.log(x)
      // })
    }
  }

  updateMatTable(res) {
    this.rows = new MatTableDataSource(res)
    this.rows.paginator = this.paginator
    this.rows.sort = this.sort
  }

  savech() {
    if (this.quSV.qu_uid != null) {
      if(this.choices.question_uid == null){
        console.log(this.questions)
        this.choices.question_uid = this.questions.question_uid
        console.log(this.choices)
        this.quSV.update2(this.questions).subscribe((x)=>{
          this.choiceSV.add(this.choices).subscribe((x) => {
            Swal.fire({
              icon: 'success',
              text: 'บันทึกข้อมูลเรียบร้อย',
              confirmButtonText: `ตกลง`,
            }).then((result) => {
              if (result.isConfirmed) {
                // this.router.navigate(['/app/structure-subject/inform-quiz'])
                this.choices.choice_uid = null
                this.choices.question_uid = null
                this.choices.choice_name = null
                this.ngOnInit()
                // this.coruseSV.lesson = this.questions
                this.cdRef.detectChanges()
              }
            })
          })
        })
       
      }else{
        console.log(this.choices)
        this.quSV.update2(this.questions).subscribe((x)=>{
          this.choiceSV.update2(this.choices).subscribe((x) => {
            Swal.fire({
              icon: 'success',
              text: 'บันทึกข้อมูลเรียบร้อย',
              confirmButtonText: `ตกลง`,
            }).then((result) => {
              if (result.isConfirmed) {
                this.choices.choice_uid = null
                this.choices.question_uid = null
                this.choices.choice_name = null
                console.log(this.choices)
                // this.router.navigate(['/app/structure-subject/inform-quiz'])
                this.ngOnInit()
                // this.coruseSV.lesson = this.questions
                this.cdRef.detectChanges()
              }
            })
          })
        })
        
      }
     
    }else{
      this.questions.course_activity_uid = this.form
      // this.quSV.qu_uid = this.questions.course_activity_uid
      console.log(this.questions)
      this.quSV.add(this.questions).subscribe((x) => {
        this.quSV.qu_uid = x.question_uid
        Swal.fire({
          icon: 'success',
          text: 'บันทึกข้อมูลเรียบร้อย',
          confirmButtonText: `ตกลง`,
        }).then((result) => {
          if (result.isConfirmed) {
            this.choices.choice_uid = null
              this.choices.question_uid = null
              this.choices.choice_name = null
            this.ngOnInit()
            this.cdRef.detectChanges()
          }
        })
      })
    }
  }

  edit(id) {
    this.choiceSV.get(id).subscribe((x:any)=>{
      console.log(x)
      this.choices = x
    })
  }

  delete(id) {
    this.choiceSV.deleteDate(id).subscribe((x:any)=>{
      Swal.fire({
        icon: 'warning',
        text: 'ต้องการลบข้อมูลนี้ใช่หรือไม่',
        confirmButtonText: `ตกลง`,
      }).then((result) => {
        if (result.isConfirmed) {
          this.ngOnInit()
          this.cdRef.detectChanges()
        }
      })
    })
  }

  back(){
    this.coruseSV.test = this.test
    this.router.navigate(['/app/structure-subject/inform'])
  }

  select(event,i,data){
    // console.log(event)
    // console.log(data)
    // console.log(i)
    this.questions.choices.map((x,index)=>{
      // console.log(x,index)
      if(i == index){
        console.log(x.is_checked)
        if(x.is_checked == true){
          x.is_checked = false
        }else{
          x.is_checked = true
        }
      }else{
        x.is_checked = false
      }
    })
    const index:any = this.questions.choices.find(x => x.is_checked == true)
    // const test:any = this.questions.choices.some(x => x.is_checked)
    // console.log(index)
    // console.log(test)
    this.questions.correct_choice_uid = index.choice_uid
    // console.log( this.data.current_question.questions[0])
    // console.log(this.choices)
    console.log(this.questions)
    
  }
  save(){
    console.log(this.questions)
    this.quSV.update2(this.questions).subscribe((x:any)=>{
      console.log(x)
      Swal.fire({
        icon: 'success',
        text: 'บันทึกเรียบร้อยแล้ว',
        confirmButtonText: `ตกลง`,
      })
    })
  }
}
