import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormInformComponent } from './form-inform.component';

describe('FormInformComponent', () => {
  let component: FormInformComponent;
  let fixture: ComponentFixture<FormInformComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormInformComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormInformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
