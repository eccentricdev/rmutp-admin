import { BaseList } from 'app/core/base/base-list';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CourseActivityService } from 'app/core/service/subject/course-activity.service';
import { CourseService } from 'app/core/service/subject/course.service';
import { QuestionsService } from 'app/core/service/subject/questions.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-inform',
  templateUrl: './form-inform.component.html',
  styleUrls: ['./form-inform.component.scss']
})
export class FormInformComponent extends BaseList implements OnInit {

  itemDatas
  form
  test
  uid
  // itemDatas:any = [{1:1}]
  displayedColumns = ['1','2','3']
  constructor(
    private coruseSV: CourseService,
    private router:Router,
    private cdRef: ChangeDetectorRef,
    private courseACSV:CourseActivityService,
    private quSV:QuestionsService
  ) {
    super();
  }

  ngOnInit(): void {
    console.log(this.coruseSV.lesson)
    console.log(this.coruseSV.test)
    this.form = this.coruseSV.lesson
    this.uid = this.coruseSV.test
    this.test = this.coruseSV.lesson
    console.log(this.form)
    if(this.coruseSV.lesson.course_activity_uid != undefined){
      console.log('lesson')
      this.courseACSV.get(this.form.course_activity_uid).subscribe((x:any)=>{
        console.log(x)
        this.itemDatas = x.questions
      })
    }else{
      console.log('test')
      this.courseACSV.get(this.uid).subscribe((x:any)=>{
        console.log(x)
        this.itemDatas = x.questions
      })
    }
    
  }

  addQu(){
    this.quSV.qu_uid = null
    this.coruseSV.quiz = this.form.course_activity_uid
    // console.log(this.form.course_activity_uid)
    this.coruseSV.test = this.form.course_activity_uid
    if(this.form.activity_uid == "7c93c33f-4361-4bf5-82fb-9a3042dd20ac") {
      //pre
      this.router.navigate(['/app/structure-subject/pre'])
    }else if(this.form.activity_uid == "b26e52e6-d453-4db1-9df8-fc0fc759cd12") {
      //post
      this.router.navigate(['/app/structure-subject/post'])
    }
    
  }

  save(){
    console.log(this.form)
    this.courseACSV.put(this.form).subscribe((x)=>{
      console.log(x)
      Swal.fire({
        icon: 'success',
        text: 'บันทึกข้อมูลเรียบร้อย',
        confirmButtonText: `ตกลง`,
      }).then((result) => {
        if (result.isConfirmed) {
          this.router.navigate(['/app/structure-subject'])
        }
      })
    })
    
  }

  clear(){
    console.log(this.form.course_uid)
    this.router.navigate(['/app/structure-subject/structure/edit',this.form.course_uid])
  }

  edit(id){
    this.quSV.qu_uid = id
    console.log(this.test)
    this.coruseSV.test = this.form.course_activity_uid
    if(this.form.activity_uid == "7c93c33f-4361-4bf5-82fb-9a3042dd20ac") {
      //pre
      this.router.navigate(['/app/structure-subject/pre'])
    }else if(this.form.activity_uid == "b26e52e6-d453-4db1-9df8-fc0fc759cd12") {
      //post
      this.router.navigate(['/app/structure-subject/post'])
    }

  }

  delete(id){
    Swal.fire({
      icon: 'warning',
      title: 'คุณต้องการลบข้อมูลนี้หรือไม่?',
      showCancelButton: true,
      confirmButtonText: `ตกลง`,
      cancelButtonText: `ยกเลิก`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.quSV.deleteDate(id).subscribe(() => {
          Swal.fire('ลบข้อมูลเรียบร้อยแล้ว', '', 'success');
          this.courseACSV.get(this.form.course_activity_uid).subscribe((x:any)=>{
            console.log(x)
            this.itemDatas = x.questions
          }),
          this.cdRef.detectChanges()
        });
      }
    });
  }
}
