import { countries } from './../../../../../mock-api/apps/contacts/data';
import { BaseForm } from './../../../../../core/base/base-form';
import { LanguageService } from '../../../../../core/service/subject/language.service';
import { SkillService } from '../../../../../core/service/subject/skill.service';
import { UploadImgService } from '../../../../../core/service/upload/upload-img.service';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { BaseTiny } from 'app/core/base/tinymce';
import { SubjectService } from 'app/core/service/subject/subject.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { SweetalertService } from 'app/core/service/sweetalert.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent extends BaseTiny implements OnInit {

  lang
  skill
  uid
  Url
  Url2
  fileUpload: FileList
  fileUpload2: FileList
  fileName: string = ''
  fileName2: string = ''
  previewImage
  previewImage2
  datainternal = {
    subject_uid:null,
    subject_code:null,
    subject_name_th:null,
    faculty_name:null,
    instructor_name:null,
    language_uid:null,
    skill_uid:null,
    is_certificate_reward:null,
    status_id:null,
    subject_description_detail:null,
    subject_purpose:null,
    subject_image:null,
    study_hour:0,
    course_number:0,
    subject_banner_image:null
  }
  constructor(
    public uploadSV: UploadImgService,
    private subjectSV:SubjectService,
    private skillSV:SkillService,
    private languageSV:LanguageService,
    private router:Router,
    private swSV: SweetalertService,
    private cdRef: ChangeDetectorRef

  ) {
    super(uploadSV);
  }

  ngOnInit(): void {
    this.languageSV.getAll().subscribe((x)=>{
      console.log(x)
      this.lang = x
    })
    this.skillSV.getAll().subscribe((x)=>{
      console.log(x)
      this.skill = x
    })
    if(this.subjectSV.subuid){
      console.log(this.subjectSV.subuid)
      this.uid = this.subjectSV.subuid
      this.subjectSV.get(this.subjectSV.subuid).subscribe((x:any)=>{
        console.log(x)
        this.datainternal.subject_uid = x?.subject_uid
        this.datainternal.subject_code = x?.subject_code
        this.datainternal.subject_name_th = x?.subject_name_th
        this.datainternal.faculty_name = x?.faculty_name
        this.datainternal.instructor_name = x?.instructor_name
        this.datainternal.language_uid = x?.language_uid
        this.datainternal.skill_uid = x?.skill_uid
        this.datainternal.is_certificate_reward = x?.is_certificate_reward
        this.datainternal.status_id = x?.status_id
        this.datainternal.subject_description_detail = x?.subject_description_detail
        this.datainternal.subject_purpose = x?.subject_purpose
        this.datainternal.study_hour = x?.study_hour
        this.datainternal.course_number = x?.course_number
        this.datainternal.subject_banner_image = x?.subject_banner_image
        this.datainternal.subject_image = x?.subject_image
        this.displaylang(x.language_uid)
        this.displayskill(x.skill_uid)
        this.subjectSV.subuid = null
      })
    }
    
  }

  clear(){
    this.router.navigate(['/app/subject'])
  }

  save(){
    console.log(this.datainternal)
    if(this.uid){
      
      this.subjectSV.update2(this.datainternal).subscribe((x)=>{
        console.log(x)
        Swal.fire({
          icon: 'success',
          text: 'บันทึกข้อมูลเรียบร้อย',
          confirmButtonText: `ตกลง`,
        }).then((result) => {
          if (result.isConfirmed) {
            this.router.navigate(['/app/subject'])
          }
        })
      })
    }else{
      this.subjectSV.add(this.datainternal).subscribe((x)=>{
        console.log(x)
        Swal.fire({
          icon: 'success',
          text: 'บันทึกข้อมูลเรียบร้อย',
          confirmButtonText: `ตกลง`,
        }).then((result) => {
          if (result.isConfirmed) {
            this.router.navigate(['/app/subject'])
          }
        })
      })
    }
    
  }

  displaylang(id) {
    console.log(id)
    if(id) return this.lang.find(lang => lang.language_uid === id)?.language_name_th
  }

  displayskill(id) {
    console.log(id)
    if(id) return this.skill.find(skill => skill.skill_uid === id)?.skill_name_th
  }

  upload(event){ 
    let files: FileList
    files = event.target.files
    let formData = new FormData()
    this.fileName = files.item(0).name
    formData.append('file',files.item(0),files.item(0).name)
    this.uploadSV.uploadDocument(formData).pipe(
      tap((res:any) => {
        if (res.status == 'success') {
          console.log(res)
          let url:string = res.message[0].file_name   
          this.Url = url 
          console.log(url)    
          // this.form.get('amenity_image_url').setValue(url)
          this.datainternal.subject_image = url
          this.cdRef.detectChanges()
          this.swSV.saveSuccess('อัพโหลดสำเร็จแล้ว')
          this.previewDoc(event)
        }      
      })
    ).subscribe()
    
  }

  previewDoc(event){
    this.upload(event.target.files)
    if (event.target.files && event.target.files[0]){
        const file = event.target.files[0];
        
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = ((event) => {
            this.previewImage = reader.result
            // console.log(this.previewImage)
            this.fileName = file.name
            this.cdRef.detectChanges()
        })
    }    
  }

  upload2(event){ 
    let files: FileList
    files = event.target.files
    let formData = new FormData()
    this.fileName2 = files.item(0).name
    formData.append('file',files.item(0),files.item(0).name)
    this.uploadSV.uploadDocument(formData).pipe(
      tap((res:any) => {
        if (res.status == 'success') {
          console.log(res)
          let url:string = res.message[0].file_name   
          this.Url2 = url 
          console.log(url)    
          this.datainternal.subject_banner_image = url
          this.cdRef.detectChanges()
          this.swSV.saveSuccess('อัพโหลดสำเร็จแล้ว')
          this.previewDoc2(event)
        }      
      })
    ).subscribe()
    // this.previewDoc2(event)
  }

  previewDoc2(event){
    this.upload(event.target.files)
    if (event.target.files && event.target.files[0]){
        const file = event.target.files[0];
        
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = ((event) => {
            this.previewImage2 = reader.result
            // console.log(this.previewImage)
            this.fileName2 = file.name
            this.cdRef.detectChanges()
        })
    }    
  }
  edit2(){
    Swal.fire({
      imageUrl: this.Url2,
      // imageHeight: 650,
      //     width: 800,
          imageAlt: 'Custom image',
    })
  }

  edit(){
    Swal.fire({
      imageUrl: this.Url,
      // imageHeight: 650,
      //     width: 800,
          imageAlt: 'Custom image',
    })
  }

  delete(){
    this.fileName = null
    this.datainternal.subject_image = null
  }

  delete2(){
    this.fileName2 = null
    this.datainternal.subject_banner_image = null
  }

  preview(){
    if(this.datainternal.subject_image != null){
      Swal.fire({
        imageUrl: this.datainternal.subject_image,
        // imageHeight: 650,
        //     width: 800,
            imageAlt: 'Custom image',
      })
    }
    
  }
  preview2(){
    if(this.datainternal.subject_banner_image != null){
      Swal.fire({
        imageUrl: this.datainternal.subject_banner_image,
        // imageHeight: 650,
        //     width: 800,
            imageAlt: 'Custom image',
      })
    }
    
  }
  
}
