
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { SubjectService } from 'app/core/service/subject/subject.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  @Output() onSearch = new EventEmitter<any>()
  fac
  subj
  search={
    subject_uid:null,
    faculty_name:null
  }
  constructor(
    private router:Router,
    private subjectSV:SubjectService
  ) { }

  ngOnInit(): void {
    this.subjectSV.getAll().subscribe((x:any)=>{
      console.log(x)
      this.subj = x
      this.fac = x
      console.log(this.fac)
    })
   
  }


  Search(){
    console.log(this.search)
    this.onSearch.emit(this.search)
  }

  add(){
    this.router.navigate(['/app/subject/add'])
  }

  displaysubj(id) {
    console.log(id)
    if(id) return this.subj.find(subj => subj.subject_uid === id)?.subject_name_th
  }
}
