import { SubjectService } from './../../../../../core/service/subject/subject.service';
import { BaseForm } from '../../../../../core/base/base-form';
import { ChangeDetectorRef, Component, Input, OnInit, SimpleChanges, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { BaseList } from 'app/core/base/base-list';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseList implements OnInit,OnChanges {

  @Input() items: any
  // itemDatas:any = [{1:1}]
  itemDatas:any=[
    {
    subject_code:'ENG3090',
    subject_name_th:'ภาษาอังกฤษเพื่อการสื่อสารในสังคม',
    faculty_name:'คณะศิลปศาสตร์',
    status_id:1
  },
  {
    subject_code:'SCI0454',
    subject_name_th:'วิทยาศาสตร์ชีวภาพ',
    faculty_name:'คณะศิลปศาสตร์',
    status_id:1
  },]
  displayedColumns = ['1','2','3','4','5','6']
  constructor(
    private router:Router,
    private cdRef: ChangeDetectorRef,
    private subjectSV:SubjectService
  ) { 
    super()
  }

  ngOnInit(): void {
    
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes?.items?.currentValue) {
      this.items = this.updateMatTable(changes.items.currentValue)
    } 
  }

  edit(id){
    // this.router.navigate(['app/fee-group/edit',id])
    this.subjectSV.subuid = id
    this.router.navigate(['/app/subject/edit',id])
  }

  delete(id){
    Swal.fire({
      icon: 'warning',
      title: 'คุณต้องการลบข้อมูลนี้หรือไม่?',
      showCancelButton: true,
      confirmButtonText: `ตกลง`,
      cancelButtonText: `ยกเลิก`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.subjectSV.deleteDate(id).subscribe(() => {
          Swal.fire('ลบข้อมูลเรียบร้อยแล้ว', '', 'success');
          this.items = this.subjectSV.getAll();
          this.cdRef.detectChanges()
        });
      }
    });
  }

}
