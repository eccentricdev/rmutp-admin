import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContainerComponent } from './container/container.component';
import { ListComponent } from './precenter/list/list.component';
import { FormComponent } from './precenter/form/form.component';
import { SearchComponent } from './precenter/search/search.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:ContainerComponent
  },
  {
    path:'add',
    component:FormComponent
  },
  {
    path:'edit/:id',
    component:FormComponent
  }
  
]

@NgModule({
  declarations: [
    ContainerComponent,
    ListComponent,
    FormComponent,
    SearchComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class CheckEaxmModule { }
