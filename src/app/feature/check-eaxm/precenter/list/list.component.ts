import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  @Input() items1: any
  @Input() items2: any
  displayedColumns1 = ['1','2','3','4','5','6']
  displayedColumns2 = ['1','2','3','4','5','6']
  @ViewChild("sort2",{ static: true }) sort2: MatSort;
  @ViewChild("bar2",{ static: true }) bar2: MatPaginator;
  constructor() { }

  ngOnInit(): void {
  }

}
