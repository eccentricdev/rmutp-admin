import { OdicOpenService } from 'app/core/service/odic/odic-open.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import Swal from 'sweetalert2';
import { AppTokenService } from 'app/core/service/token/app-token.service';

@Component({
  selector: 'app-authen',
  templateUrl: './authen.component.html',
  styleUrls: ['./authen.component.scss']
})
export class AuthenComponent implements OnInit {

  constructor(
    private odic: OdicOpenService,
    private router: Router,
    private store: Store,
    private apptokenSV:AppTokenService
  ) { }

  ngOnInit(): void {
    this.odic.manager.signinRedirectCallback().then(res => {
      console.log(res)      
      this.odic.user = res
      this.router.navigate(['/app/subject'])
      // this.apptokenSV.getAll().subscribe((x)=>{
        
      //   // Swal.fire({
      //   //   imageUrl: 'assets/images/popup.png',
      //   //   imageHeight: 650,
      //   //   width: 800,
      //   //   imageAlt: 'Custom image',
      //   //   confirmButtonText: 'ตกลง',
      //   //   // confirmButtonColor: '#171D6B',
      //   // })
      //   this.router.navigate(['/app/subject'])
      // })
    }).catch(err => {

    })
    

  }

}
