import { BaseForm } from './../../../../core/base/base-form';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SweetalertService } from 'app/core/service/sweetalert.service';
import { SkillService } from 'app/core/service/subject/skill.service';
import { tap, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent extends BaseForm implements OnInit {
  datainternal={
    skill_code:null,
    skill_name_th:null,
    skill_name_en:null,
    status_id:null
  }
  constructor(
    public activeRouter: ActivatedRoute,
    public fb: FormBuilder,
    private router:Router,
    private swSV: SweetalertService,
    private cdRef: ChangeDetectorRef,
    private skillSV:SkillService
  ) {
    super(fb,activeRouter);
  }

  ngOnInit(): void {
    console.log(this.state)
    switch (this.state) {
      case 'edit':
        this.skillSV.get(this.id).subscribe((res: any) => {
          console.log(res);
          this.datainternal = res;
          this.cdRef.detectChanges();
        })
        break;

      case 'add':
        
      break
    }
  }

  save(){
    switch (this.state) {
      case 'edit':
        this.skillSV.update2(this.datainternal).pipe(
          tap(res => this.swSV.saveSuccess()),
          tap(() => this.clear()),
          tap(() => this.cdRef.detectChanges()),
          catchError(err => {
            this.swSV.error()
            return throwError(err)
          })
        ).subscribe()
        break;

      case 'add':
        console.log(this.datainternal)
        this.skillSV.add(this.datainternal).pipe(
          tap(res => this.swSV.saveSuccess()),
          tap(() => this.clear()),
          tap(() => this.cdRef.detectChanges()),
          catchError(err => {
            this.swSV.error()
            return throwError(err)
          })
        ).subscribe()
        break
    }
  }

  clear(){
    this.router.navigate(['/app/skill'])
  }
}
