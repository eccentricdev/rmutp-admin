import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { SkillService } from 'app/core/service/subject/skill.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  @Output() onSearch = new EventEmitter<any>()
  skill
  search={
    skill_uid:null
  }
  constructor(
    private router:Router,
    private skillSV:SkillService
  ) { }

  ngOnInit(): void {
    this.skillSV.getAll().subscribe((x:any)=>{
      this.skill = x
    })
  }

  Search(){
    console.log(this.search)
    this.onSearch.emit(this.search)
  }

  add(){
    this.router.navigate(['/app/skill/add'])
  }

  displayskill(id) {
    console.log(id)
    if(id) return this.skill.find(skill => skill.skill_uid === id)?.skill_name_th
  }
}
