import { Component, OnInit } from '@angular/core';
import { SkillService } from 'app/core/service/subject/skill.service';
import { createQueryStringFromObject } from 'app/shared/util/func';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit {

  items$ = new Observable()
  constructor(
    private skillSV:SkillService
  ) { }

  ngOnInit(): void {
    this.items$ = this.skillSV.getAll()
  }

  search(query: string){
    let queryStr = createQueryStringFromObject(query)
    if (queryStr) {
      this.items$ = this.skillSV.query(`?${queryStr}`)
    } else {
      this.items$ = this.skillSV.getAll()
    }
  }

}
