import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContainerComponent } from './container/container.component';
import { ListComponent } from './presenter/list/list.component';
import { FormComponent } from './presenter/form/form.component';
import { ListRegisterComponent } from './presenter/list-register/list-register.component';
import { SearchComponent } from './presenter/search/search.component';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { AddformComponent } from './presenter/addform/addform.component';
import { ListUserComponent } from './presenter/list-user/list-user.component';

const routes:Routes = [
  {
    path:'',
    component:ContainerComponent
  },
  {
    path:'add',
    component:AddformComponent
  },
  {
    path:'edit',
    component:FormComponent
  },
  {
    // path:'edit/:id',
    path:'list-regis',
    component:ListRegisterComponent
  },
  {
    // path:'edit/:id',
    path:'list-register',
    component:ListUserComponent
  },
  
]

@NgModule({
  declarations: [
    ContainerComponent,
    ListComponent,
    FormComponent,
    ListRegisterComponent,
    SearchComponent,
    AddformComponent,
    ListUserComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class ManageStudentModule { }
