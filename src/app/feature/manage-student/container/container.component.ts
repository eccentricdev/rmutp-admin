import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { SubjectService } from 'app/core/service/subject/subject.service';
import { createQueryStringFromObject } from 'app/shared/util/func';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit {

  items$ = new Observable()
  constructor(
    private subjectSV:SubjectService
  ) { }

  ngOnInit(): void {
    this.items$ = this.subjectSV.getAll()
  }

  search(query: string){
    let queryStr = createQueryStringFromObject(query)
    if (queryStr) {
      this.items$ = this.subjectSV.query(`?${queryStr}`)
    } else {
      this.items$ = this.subjectSV.getAll()
    }
  }

}
