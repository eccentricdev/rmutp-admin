import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { BaseList } from 'app/core/base/base-list';
import { SubjectService } from 'app/core/service/subject/subject.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseList implements OnInit,OnChanges {

  @Input() items: any
  // itemDatas:any = [{1:1}]
  itemDatas:any=[
    {
    subject_code:'ENG3090',
    subject_name_th:'ภาษาอังกฤษเพื่อการสื่อสารในสังคม',
    faculty_name:'คณะศิลปศาสตร์',
    status_id:1
  },
  {
    subject_code:'SCI0454',
    subject_name_th:'วิทยาศาสตร์ชีวภาพ',
    faculty_name:'คณะศิลปศาสตร์',
    status_id:1
  },
]
  displayedColumns = ['1','2','3','4','5','6']
  constructor(
    private router:Router,
    private cdRef: ChangeDetectorRef,
    private subjectSV:SubjectService
  ) { 
    super()
  }

  ngOnInit(): void {
    console.log(this.items)
   
    if(this.items == null){
      console.log(this.items)
      this.items = this.itemDatas
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    // if(changes?.items?.currentValue) {
    //   this.items = this.updateMatTable(changes.items.currentValue)
    // } 
    console.log(this.items)
   
    if(this.items?.length == 0){
      console.log(this.items)
      this.items = this.itemDatas
    }
  }

  edit(id){
    console.log(id)
    this.subjectSV.Subjectuid = id
    this.router.navigate(['/app/manage-student/list-register'])
  }

  delete(id){
    Swal.fire({
      icon: 'warning',
      title: 'คุณต้องการลบข้อมูลนี้หรือไม่?',
      showCancelButton: true,
      confirmButtonText: `ตกลง`,
      cancelButtonText: `ยกเลิก`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.subjectSV.deleteDate(id).subscribe(() => {
          Swal.fire('ลบข้อมูลเรียบร้อยแล้ว', '', 'success');
          this.items = this.subjectSV.getAll();
          this.cdRef.detectChanges()
        });
      }
    });
  }

}
