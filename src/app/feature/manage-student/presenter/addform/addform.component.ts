import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-addform',
  templateUrl: './addform.component.html',
  styleUrls: ['./addform.component.scss']
})
export class AddformComponent implements OnInit {

  form={
    rgisNum:null,
    numall:null,
    type:null,
    day1:null,
    day2:null
  }
  itemDatas:any 
  displayedColumns = ['1','2','3','4','5','6']
  constructor(
    private router:Router,
  ) { }

  ngOnInit(): void {
  }

  save(){
    this.router.navigate(['/app/manage-student'])
  }
}
