import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  form={
    rgisNum:1,
    numall:100,
    type:'นักศึกษา',
    day1:'10 พ.ค. 65',
    day2:'10 เม.ย. 65'
  }
  itemDatas:any =[
    {
      code:650089766,
      name:'นางสาวศรีสุภา ศรีเงิน',
      fac:'บริหารธุรกิจ',
      major:'การจัดการการเงิน',
      Progress:75
    },
    {
      code:650089767,
      name:'นายเกียรติภูมิ ทรัพย์รุ่งโรจน์',
      fac:'บริหารธุรกิจ',
      major:'การจัดการการเงิน',
      Progress:40
    },
  ]
  displayedColumns = ['1','2','3','4','5','6']
  constructor(
    private router:Router,
  ) { }

  ngOnInit(): void {
  }

  save(){
    this.router.navigate(['/app/manage-student'])
  }
}
