import { BaseList } from './../../../../core/base/base-list';
import { Component, OnInit } from '@angular/core';
import { TetsSubjectService } from 'app/core/service/test/tets-subject.service';
import { SubjectService } from 'app/core/service/subject/subject.service';
import { createQueryStringFromObject } from 'app/shared/util/func';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss']
})
export class ListUserComponent extends BaseList implements OnInit {

  itemDatas:any
  displayedColumns = ['1','3','6','7']
  // displayedColumns = ['1','2','3','4','5','6']
  uid ={
    subject_uid:null
  }
  constructor(
    private testSubSV:TetsSubjectService,
    private subjectSV:SubjectService
  ) {
    super();
  }

  ngOnInit(): void {
    this.uid.subject_uid = this.subjectSV.Subjectuid
    let queryStr = createQueryStringFromObject(this.uid)
    this.testSubSV.queryString(queryStr).subscribe((x:any)=>{
      console.log(x)
      this.itemDatas = x
    })
  }

}
