import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { BaseList } from 'app/core/base/base-list';

@Component({
  selector: 'app-list-register',
  templateUrl: './list-register.component.html',
  styleUrls: ['./list-register.component.scss']
})
export class ListRegisterComponent extends BaseList implements OnInit,OnChanges {
  items:any =[
    {
      day1:'10 เม.ย. 65',
      day2:'20 เม.ย. 65',
      type:'นักศึกษา',
      num1:100,
      num2:25,

    },
    {
      day1:'10 พ.ค. 65',
      day2:'20 พ.ค. 65',
      type:'บุคคลทั่วไป',
      num1:100,
      num2:5,

    },
  ]
  itemDatas:any = [{1:1}]
  displayedColumns = ['1','2','3','4','5','6','7']

  constructor(private router:Router,) {
    super();
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes?.items?.currentValue) {
      this.items = this.updateMatTable(changes.items.currentValue)
    } 
  }


  Search(){

    this.router.navigate(['/app/manage-student/add'])
  }

  edit(){
    this.router.navigate(['/app/manage-student/edit'])
  }

}
