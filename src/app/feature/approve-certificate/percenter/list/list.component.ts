import { MatTableDataSource } from '@angular/material/table';
import { BaseList } from './../../../../core/base/base-list';
import { Component, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseList implements OnInit {

  @Input() items1: any
  @Input() items2: any
  @Input() items3: any
  displayedColumns1 = ['1','2','3','4','5','6']
  displayedColumns2 = ['1','2','3','4','5','6']
  displayedColumns3 = ['1','2','3','4','5','6']
  @ViewChild("sort2",{ static: true }) sort2: MatSort;
  @ViewChild("bar2",{ static: true }) bar2: MatPaginator;
  @ViewChild("sort3",{ static: true }) sort3: MatSort;
  @ViewChild("bar3",{ static: true }) bar3: MatPaginator;
  constructor() {
    super();
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes)
    if (changes?.items1?.currentValue ) {      
      this.items1 = this.updateMatTable(changes.payment.currentValue)
      console.log(this.items1)
      
    } 
    if(changes?.items2?.currentValue) {
      //this.receipt = this.updateMatTable(changes.receipt.currentValue)
        this.items2 = new MatTableDataSource(changes.receipt.currentValue)
        this.items2.paginator = this.bar2
        this.items2.sort = this.sort2   
      console.log(this.items2)
    } 
    if(changes?.items3?.currentValue) {
      //this.receipt = this.updateMatTable(changes.receipt.currentValue)
        this.items3 = new MatTableDataSource(changes.receipt.currentValue)
        this.items3.paginator = this.bar3
        this.items3.sort = this.sort3   
      console.log(this.items3)
    } 
  }

}
