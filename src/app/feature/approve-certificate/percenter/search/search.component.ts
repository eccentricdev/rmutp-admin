import { Router } from '@angular/router';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  @Output() onSearch = new EventEmitter<any>()
  fac
  subj
  search={
    subject_uid:null,
    faculty_name:null
  }
  constructor(
    private router:Router,
  ) { }

  ngOnInit(): void {
  }

  Search(){
    console.log(this.search)
    this.onSearch.emit(this.search)
  }


  displaysubj(id) {
    console.log(id)
    if(id) return this.subj.find(subj => subj.subject_uid === id)?.subject_name_th
  }
}
