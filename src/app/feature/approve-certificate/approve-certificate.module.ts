import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContainerComponent } from './container/container.component';
import { SearchComponent } from './percenter/search/search.component';
import { ListComponent } from './percenter/list/list.component';
import { FormComponent } from './percenter/form/form.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:ContainerComponent
  },
  {
    path:'add',
    component:FormComponent
  },
  {
    path:'edit/:id',
    component:FormComponent
  }
  
]

@NgModule({
  declarations: [
    ContainerComponent,
    SearchComponent,
    ListComponent,
    FormComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class ApproveCertificateModule { }
