import { TestCerfiticateService } from './../../../core/service/test/test-cerfiticate.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { query } from '@angular/animations';
import { createQueryStringFromObject } from 'app/shared/util/func';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit {

  items1$ = new Observable()
  items2$ = new Observable()
  items3$ = new Observable()
  constructor(
    private testCerSV:TestCerfiticateService
  ) { }

  ngOnInit(): void {
    this.items1$ = this.testCerSV.waiting_list()
    this.items2$ = this.testCerSV.getAll().pipe(
      map((res:any)=>res.filter(x=>x.is_agree == true))
    )
    this.items3$ = this.testCerSV.getAll().pipe(
      map((res:any)=>res.filter(x=>x.is_agree == false))
    )
  }

  search(query: string){
    let queryStr = createQueryStringFromObject(query)
      console.log(queryStr)
      // if (queryStr) {
      //   this.items1$ = this.testCerSV.queryString(queryStr).pipe(
      //     map((res:any)=>res.filter(x=>x.is_agree == true))
      //   )
           
      // } else {
      //   this.items1$ = this.testCerSV.waiting_list()    
      // }

      if (queryStr) {
        this.items2$ = this.testCerSV.queryString(queryStr).pipe(
          map((res:any)=>res.filter(x=>x.is_agree == true))
        )
           
      } else {
        this.items2$ = this.testCerSV.getAll().pipe(
          map((res:any)=>res.filter(x=>x.is_agree == true))
        )     
      }

      if (queryStr) {
        this.items3$ = this.testCerSV.queryString(queryStr).pipe(
          map((res:any)=>res.filter(x=>x.is_agree == false))
        )
           
      } else {
        this.items3$ = this.testCerSV.getAll().pipe(
          map((res:any)=>res.filter(x=>x.is_agree == false))
        )     
      }
  }

}
