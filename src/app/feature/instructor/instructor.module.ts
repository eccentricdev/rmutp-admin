import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContainerComponent } from './container/container.component';
import { ListComponent } from './presenter/list/list.component';
import { FormComponent } from './presenter/form/form.component';
import { SearchComponent } from './presenter/search/search.component';
import { SharedModule } from 'app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:ContainerComponent
  },
  {
    path:'add',
    component:FormComponent
  },
  {
    path:'edit/:id',
    component:FormComponent
  }
  
]

@NgModule({
  declarations: [
    ContainerComponent,
    ListComponent,
    FormComponent,
    SearchComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class InstructorModule { }
