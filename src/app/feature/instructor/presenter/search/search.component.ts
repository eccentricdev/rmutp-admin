import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  @Output() onSearch = new EventEmitter<any>()
  fac
  subj
  search={
    subject_uid:null,
    faculty_name:null
  }
  constructor() { }

  ngOnInit(): void {
  }

  Search(){
    console.log(this.search)
    this.onSearch.emit(this.search)
  }

}
