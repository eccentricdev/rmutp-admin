import { ProgramsTypeState, programeTypeReducer } from './../core/core-state/program-types/reducer/programs-type.reducer';
import {  educationLevelReducer, EducationLevelState } from './../core/core-state/education-level/reducer/education-level.reducer';

import { ActionReducerMap } from '@ngrx/store';
import { ProgramState, programeReducer } from '../core/core-state/programes/reducers/programe.reducer';
import { StepperState, stepperReducer } from '../core/core-state/stepper/reducers/stepper.reducer';
import { RSUState, appStateReducer } from '../core/core-state/app-state/reducers/appstate.reducer';
import { PrefixState, prefixsReducer } from '../core/core-state/prefix/reducers/prefix.reducer';
import { forgetPasswordReducer, ForgetPasswordState } from '../core/core-state/forget-password/reducer/forget-password.reducer';
import { userReducer, UserState } from '../core/core-state/authen/reducers';


export interface AppState {
    user: UserState,
    programe: ProgramState
    stepper: StepperState,
    rsu: RSUState,
    prefix: PrefixState,
    educationLevel: EducationLevelState,
    programsType: ProgramsTypeState,
    forgetPassword: ForgetPasswordState
}

export const appReducers: ActionReducerMap<AppState> = {
    user: userReducer,
    programe: programeReducer,
    stepper: stepperReducer,
    rsu: appStateReducer,
    prefix: prefixsReducer,
    educationLevel: educationLevelReducer,
    programsType: programeTypeReducer,
    forgetPassword: forgetPasswordReducer,
}