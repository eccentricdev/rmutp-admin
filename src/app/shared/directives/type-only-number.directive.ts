
import { Directive, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: '[TypeOnlyNumber]'
})
export class TypeOnlyNumberDirective {
  private el: HTMLInputElement;
  constructor(
    private elementRef: ElementRef
  ) {
    this.el = this.elementRef.nativeElement
  }

  @HostListener('keypress',['$event'])
  onTyping(event){
    // console.log(event)
    if (event.charCode > 31 && (event.charCode < 48 || event.charCode > 57)) {
      return false;
    }
    return true;
  }

}
