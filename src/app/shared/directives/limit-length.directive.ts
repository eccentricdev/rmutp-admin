import { Directive, Input, ElementRef, HostListener } from '@angular/core';
// import { NgControl } from '@angular/forms';
// import { NgControl } from '@angular/forms';

@Directive({
  selector: '[LimitLength]'
})
export class LimitLengthDirective {
  @Input() limitLength:number = 0
  private el:HTMLInputElement   
  constructor(  
    private elementRef :ElementRef,
    // private controls:NgControl
  ){
    this.el = this.elementRef.nativeElement;    
  }
  
  @HostListener('keypress',['$event']) type(value){    
    if(value.target.value.length >= this.limitLength) {
      // this.elementRef.nativeElement.value = ''
      return false
    }
  }
}
