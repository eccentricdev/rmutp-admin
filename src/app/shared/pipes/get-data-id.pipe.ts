import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getDataId'
})
export class GetDataIdPipe implements PipeTransform {

  defaultUID = null
  transform(id: any, items: Array<any>, keyArray: string, key: string, msg: any, defaultMsg?: string): any{
    // console.log(id)
    // console.log(items)
    // console.log(keyArray)
    // console.log(key)
    if (id == this.defaultUID && defaultMsg != undefined) {      
      return msg
    } else if (id) {
      if(items != undefined){
        let index = items.findIndex(x => x[keyArray] == id)            
        if(index == -1){
          return msg
        } else {
          return index != -1 ? items[index][key] : msg
        }
      }      
    } else {
      return msg
    }
    console.log(msg);
    
  }

}
