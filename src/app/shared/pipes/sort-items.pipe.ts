import { Pipe, PipeTransform } from '@angular/core';
import { sortByProperty } from 'app/core/base/base-form';

@Pipe({
  name: 'sortItems',
  pure: false
})
export class SortItemsPipe implements PipeTransform {

  transform(items: Array<any>,file:any): any {
    // console.log(file)
    // console.log(items.values)
    if(items){
      let itemsSort
      return  itemsSort = items.sort(sortByProperty(file));
    }
    
  }

}
