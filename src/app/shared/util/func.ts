import * as moment from 'moment'

export const thaiTimeZone = (date:  Date | moment.Moment): Date => {
    return moment(date).add(7,'hours').toDate()
} 

export const createQueryStringFromObject = (object: any):string => {
    let queryStr: string = ''
  
    for (const key in object) {
        if (object[key]) {
            queryStr += `${key}=${object[key]}&`
        }
    }
    let lastIndex = queryStr.lastIndexOf('&')
    queryStr = queryStr.slice(0,lastIndex)
    return queryStr
  }