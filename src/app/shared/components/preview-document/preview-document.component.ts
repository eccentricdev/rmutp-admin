import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'preview-document',
  templateUrl: './preview-document.component.html',
  styleUrls: ['./preview-document.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreviewDocumentComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<PreviewDocumentComponent>,
    @Inject(MAT_DIALOG_DATA) public urlDoc: string,
  ) { }

  ngOnInit(): void {
    this.urlDoc = this.urlDoc.replace('http','https')
    
  }

  closePopup(){
    this.dialogRef.close()
  }

}
