import { AbstractControl } from '@angular/forms';

export function graduateValidator(control: AbstractControl): { [key: string]: boolean } | null {
    if (+control.value < 2500) {
        return { lessThan: true}
    }

    if (+control.value > 2699) {
        return { greaterThan: true }
    }

    return  null
}