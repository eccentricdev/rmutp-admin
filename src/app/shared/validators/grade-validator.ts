import { AbstractControl } from '@angular/forms';

export function gradeValidator(control: AbstractControl): { [key: string]: boolean } | null {
    let condition = /^\d+\.\d{0,2}$/
    let isMatch = new RegExp(condition).test(control.value)
    // console.log(isMatch)
    if (!isMatch) {
        return {notMatch: true}
    }
    return  null
}