/* tslint:disable:max-line-length */
import { FuseNavigationItem } from '@fuse/components/navigation';

export const defaultNavigation: FuseNavigationItem[] = [
    {
        id      : 'การจัดการบทเรียน',
        title   : 'การจัดการบทเรียน',
        subtitle: '',
        type    : 'collapsable',
        // icon    : 'heroicons_outline:home',
        children: [
            {
                id   : 'รายละเอียดModule',
                title: 'รายละเอียดModule',
                type : 'basic',
                // icon : 'heroicons_outline:clipboard-check',
                link : '/app/subject',
                // exactMatch: true
            }
            ,
            {
                id   : 'โครงสร้างModule',
                title: 'โครงสร้างModule',
                type : 'basic',
                // icon : 'heroicons_outline:clipboard-check',
                link : '/app/structure-subject',
                // exactMatch: true
            }
        ]
    },
    {
        id      : 'การจัดการนักศึกษา',
        title   : 'การจัดการนักศึกษา',
        subtitle: '',
        type    : 'collapsable',
        // icon    : 'heroicons_outline:home',
        children: [
            {
                id   : 'จัดการรอบและนักศึกษาที่ลงทะเบียน',
                title: 'จัดการรอบและนักศึกษาที่ลงทะเบียน',
                type : 'basic',
                // icon : 'heroicons_outline:clipboard-check',
                link : '/app/manage-student',
                // exactMatch: true
            },
            {
                id   : 'อนุมัติใบประกาศ',
                title: 'อนุมัติใบประกาศ',
                type : 'basic',
                // icon : 'heroicons_outline:clipboard-check',
                link : '/app/approve-certificate',
                // exactMatch: true
            },
            {
                id   : 'ตรวจแบบฝึกหัด',
                title: 'ตรวจแบบฝึกหัด',
                type : 'basic',
                // icon : 'heroicons_outline:clipboard-check',
                link : '/app/check-eaxm',
                // exactMatch: true
            }
        ]
    },
    {
        id      : 'ข้อมูลตั้งต้น',
        title   : 'ข้อมูลตั้งต้น',
        subtitle: '',
        type    : 'collapsable',
        // icon    : 'heroicons_outline:home',
        children: [
            {
                id   : 'ภาษาที่ใช้',
                title: 'ภาษาที่ใช้',
                type : 'basic',
                // icon : 'heroicons_outline:clipboard-check',
                link : '/app/language',
                // exactMatch: true
            },
            {
                id   : 'ทักษะผู้เรียน',
                title: 'ทักษะผู้เรียน',
                type : 'basic',
                // icon : 'heroicons_outline:clipboard-check',
                link : '/app/skill',
                // exactMatch: true
            },
            {
                id   : 'อาจารย์ผู้สอน',
                title: 'อาจารย์ผู้สอน',
                type : 'basic',
                // icon : 'heroicons_outline:clipboard-check',
                link : '/app/instructor',
                // exactMatch: true
            },
        ]
    },
];
export const compactNavigation: FuseNavigationItem[] = [
    {
        id   : 'example',
        title: 'Example',
        type : 'basic',
        icon : 'heroicons_outline:chart-pie',
        link : '/example'
    }
];
export const futuristicNavigation: FuseNavigationItem[] = [
    {
        id   : 'example',
        title: 'Example',
        type : 'basic',
        icon : 'heroicons_outline:chart-pie',
        link : '/example'
    }
];
export const horizontalNavigation: FuseNavigationItem[] = [
    {
        id   : 'example',
        title: 'Example',
        type : 'basic',
        icon : 'heroicons_outline:chart-pie',
        link : '/example'
    }
];
