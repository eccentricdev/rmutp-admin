import { ApproveCertificateModule } from './feature/approve-certificate/approve-certificate.module';
import { LanguageModule } from 'app/layout/common/language/language.module';
import { Route } from '@angular/router';

import { LayoutComponent } from 'app/layout/layout.component';
import { InitialDataResolver } from 'app/app.resolvers';
import { AuthGuard } from './core/guards/auth.guard';

export const appRoutes: Route[] = [

    // Redirect empty path to '/example'
    {path: '', pathMatch : 'full', redirectTo: '/app/example'},

    // Redirect signed in user to the '/example'
    //
    // After the user signs in, the sign in page will redirect the user to the 'signed-in-redirect'
    // path. Below is another redirection for that path to redirect the user to the desired
    // location. This is a small convenience to keep all main routes together here on this file.
    {path: 'signed-in-redirect', pathMatch : 'full', redirectTo: '/app/example'},
    {
        path: 'auth-callback',
        loadChildren: () => import('./feature/authen/authen.module')
          .then(res => res.AuthenModule)
      },
    // Admin routes
    {
        path       : 'app',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        component  : LayoutComponent,
        resolve    : {
            initialData: InitialDataResolver,
        },
        children   : [
            {
                path: 'example', 
                loadChildren: () => import('app/modules/admin/example/example.module')
                .then(m => m.ExampleModule)
            },
            {
                path: 'subject', 
                loadChildren: () => import('./feature/subjects/subject/subject.module')
                .then(m => m.SubjectModule)
            },
            {
                path: 'structure-subject', 
                loadChildren: () => import('./feature/subjects/structure/structure.module')
                .then(m => m.StructureModule)
            },
            {
                path: 'manage-student', 
                loadChildren: () => import('./feature/manage-student/manage-student.module')
                .then(m => m.ManageStudentModule)
            },
            {
                path: 'skill', 
                loadChildren: () => import('./feature/setup-skill/setup-skill.module')
                .then(m => m.SetupSkillModule)
            },
            {
                path: 'language', 
                loadChildren: () => import('./feature/setup-language/setup-language.module')
                .then(m => m.SetupLanguageModule)
            },
            {
                path: 'approve-certificate', 
                loadChildren: () => import('./feature/approve-certificate/approve-certificate.module')
                .then(m => m.ApproveCertificateModule)
            },
            {
                path: 'check-eaxm', 
                loadChildren: () => import('./feature/check-eaxm/check-eaxm.module')
                .then(m => m.CheckEaxmModule)
            },
            {
                path: 'instructor', 
                loadChildren: () => import('./feature/instructor/instructor.module')
                .then(m => m.InstructorModule)
            }
        ]
    }
];
