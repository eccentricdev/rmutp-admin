import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from 'app/core/base/base-service';

@Injectable({
  providedIn: 'root'
})
export class TestCerfiticateService extends BaseService {


  constructor(public http : HttpClient) {
    super('/test/test_certificate/',http);
  }

  waiting_list(){
    return this.http.get(`https://rmutp.71dev.com/rmutp-mooc-api/api/test/test_certificate/waiting_list`)
  }
}
