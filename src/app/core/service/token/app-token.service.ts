import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { delay, tap } from 'rxjs/operators';
import { JwtHelperService } from "@auth0/angular-jwt";


export interface Payload {
  user_uid: string;
  login_name: string;
  display_name: string;
  agency_uid: string;
  exp: number;
  iss: string;
  aud: string;
}

@Injectable({
  providedIn: 'root'
})
export class AppTokenService {

  tokenUser
  decodeToken
  constructor(public http : HttpClient) { }

  getAll(){
    return this.http.get(`https://rmutp.71dev.com/rmutp-mooc-api/api/security/app_tokens`).pipe( 
      tap((res: any) => {
          this.tokenUser = res
          const helper = new JwtHelperService();
          this.decodeToken = helper.decodeToken(res.token);
          console.log(this.decodeToken)
      }),
      
      tap(res=> console.log(res)),
      
    )
  }



  private _decodeToken(): Payload {    
    const helper = new JwtHelperService();
    this.decodeToken = helper.decodeToken(this.tokenUser?.token);
    return helper.decodeToken(this.tokenUser?.token);
  }






}
