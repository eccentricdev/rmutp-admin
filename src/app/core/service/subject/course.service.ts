import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from 'app/core/base/base-service';

@Injectable({
  providedIn: 'root'
})
export class CourseService extends BaseService {

  lesson
  quiz
  test
  constructor(public http : HttpClient) {
    super('/subject/course',http);
  }

  put(data){
    return this.http.put(`https://rmutp.71dev.com/rmutp-mooc-api/api/subject/course`,data)
  }
}


