import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './../../base/base-service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SubjectService extends BaseService {


  constructor(public http : HttpClient) {
    super('/subject/subject',http);
  }

  Subjectuid
  subuid
}
