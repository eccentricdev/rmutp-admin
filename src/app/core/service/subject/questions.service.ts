import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from 'app/core/base/base-service';

@Injectable({
  providedIn: 'root'
})
export class QuestionsService extends BaseService {
  qu
  qu_uid = null
  constructor(public http : HttpClient) {
    super('/subject/question',http);
  }

  put(data){
    return this.http.put(`https://rmutp.71dev.com/rmutp-mooc-api/api/subject/question`,data)
  }
}
