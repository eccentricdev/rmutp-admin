import { BaseService } from './../../base/base-service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ChoicesService extends BaseService {


  constructor(public http : HttpClient) {
    super('/subject/choice',http);
  }
}
